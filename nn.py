import csv
import scipy.io
import numpy as np
import math
import random

import tensorflow as tf
from tensorflow import keras

from keras.models import Sequential
from keras.layers import Dense
from keras.activations import softmax

import itertools
from sklearn.metrics import confusion_matrix

y = []

with open('connect-4.data', newline='') as csvfile:
    reader = csv.reader(csvfile, delimiter=',')
    for row in reader:
        singleInsert = []
        for column in row:
            #transform field to int [0, 2]
            if(column == 'x'):
                column = 2
            elif(column == 'b'):
                column = 1
            elif(column == 'o'):
                column = 0

            singleInsert.append(column)

        #transform result to int [0, 2]
        """ if(singleInsert[42] == 'win'):
            singleInsert[42] = 2
        elif(singleInsert[42] == 'draw'):
            singleInsert[42] = 1
        elif(singleInsert[42] == 'loss'):
            singleInsert[42] = 0 """
        if(singleInsert[42] == 'win'):
            singleInsert.remove('win')
            singleInsert.append(1)
            singleInsert.append(0)
            singleInsert.append(0)
        elif(singleInsert[42] == 'draw'):
            singleInsert.remove('draw')
            singleInsert.append(0)
            singleInsert.append(1)
            singleInsert.append(0)
        elif(singleInsert[42] == 'loss'):
            singleInsert.remove('loss')
            singleInsert.append(0)
            singleInsert.append(0)
            singleInsert.append(1)
        
        # print(len(singleInsert))
        y.append(singleInsert)

x = random.sample(list(y), len(y))
ll = []
for i in range(0,42):
    ll.append(i)

def getModel():
    model = Sequential()
    model.add(Dense(40, input_dim=42, activation='relu'))
    model.add(Dense(30, activation='relu'))
    model.add(Dense(10, activation='relu'))
    model.add(Dense(5, activation='relu'))
    model.add(Dense(3, activation='softmax'))
    model.compile(loss='mean_squared_error', optimizer="adam", metrics=['accuracy'])
    return model

#editable factor
maxK = 10
Ks = [2, 5, 10]

global_accuracies = []
for K in Ks:
    print("K = %d" % (K))

    #split data into k groups
    inputGroups = []
    labelGroups = []
    for i in range(0, K):
        startIndex = float(i)/K
        endIndex = startIndex + float(1)/K
        group = np.array(x[int(len(x) * startIndex) : int(len(x) * endIndex)])
        inputGroups.append(group[:,ll])
        labelGroups.append(group[:,[42,43,44]])

    accuracies = []
    train_precisions = []
    train_recalls = []
    test_precisions = []
    test_recalls = []
    for i in range(0, maxK):
        print("index = %d/%d" % (i+1, K))

        #manage sets
        pre_train_input = []
        pre_train_label = []
        pre_test_input = []
        pre_test_label = []
        for j in range(0, K):
            if (j == i%K):
                test_input = inputGroups[i%K]
                test_label = labelGroups[i%K]
            else:
                pre_train_input.extend(inputGroups[i%K].tolist())
                pre_train_label.extend(labelGroups[i%K].tolist())

        model = getModel()

        train_input = np.array(pre_train_input)
        train_label = np.array(pre_train_label)

        history = model.fit(train_input, train_label, epochs=1, batch_size=10, verbose=2)#edit epochs!
        scores = model.evaluate(train_input, train_label)

        accuracies.append(scores[1]*100)#add accuracy

        predictions_train = model.predict(train_input)
        #format predictions into [0, 2] format
        formattedPred = []
        for pred in predictions_train:
            regList = np.ndarray.tolist(pred)
            formattedPred.append(regList.index(max(regList)))
        #format labels into [0, 2] format
        formattedLabels = []
        for nprow in train_label:
            row = nprow.tolist()
            formattedLabels.append(row.index(max(row)))

        cm = confusion_matrix(formattedPred,formattedLabels)
        prec = cm[0][0] / (cm[0][0] + cm[0][1] + cm[0][2]) * 100
        rec = cm[0][0] / (cm[0][0] + cm[1][0] + cm[2][0]) * 100
        train_precisions.append(prec)
        train_recalls.append(rec)

        predictions_test = model.predict(test_input)
        #format predictions into [0, 2] format
        formattedPred = []
        for pred in predictions_test:
            regList = np.ndarray.tolist(pred)
            formattedPred.append(regList.index(max(regList)))
        #format labels into [0, 2] format
        formattedLabels = []
        for nprow in test_label:
            row = nprow.tolist()
            formattedLabels.append(row.index(max(row)))

        cm = confusion_matrix(formattedPred,formattedLabels)
        prec = cm[0][0] / (cm[0][0] + cm[0][1] + cm[0][2]) * 100
        rec = cm[0][0] / (cm[0][0] + cm[1][0] + cm[2][0]) * 100
        test_precisions.append(prec)
        test_recalls.append(rec)

    accuracy = float(sum(accuracies))/len(accuracies)
    print("\naccuracy: %.2f%%" % (accuracy))

    global_accuracies.append(accuracy)#deciding factor

    #print results for train-set
    precision = float(sum(train_precisions))/len(train_precisions)
    recall = float(sum(train_recalls))/len(train_recalls)
    print("\ntrain-set precision: %.2f%%" % (precision))
    print("train-set recall: %.2f%%" % (recall))

    #print results for test-set
    precision = float(sum(test_precisions))/len(test_precisions)
    recall = float(sum(test_recalls))/len(test_recalls)
    print("\ntest-set precision: %.2f%%" % (precision))
    print("test-set recall: %.2f%%" % (recall))

bestK = global_accuracies.index(max(global_accuracies))
print("\nBest K is %d with accuraccy: %.2f%%" % (Ks[bestK], global_accuracies[bestK]))

################## final K calculated

train = np.array(x[:int(len(x)*float(Ks[bestK]-1)/Ks[bestK])])
test = np.array(x[int(len(x)*float(Ks[bestK]-1)/Ks[bestK]):])

train_input = train[:,ll]
train_label = train[:,[42,43,44]]
test_input = test[:,ll]
test_label = test[:,[42,43,44]]

def as_keras_metric(method):
    import functools
    from keras import backend as K
    import tensorflow as tf
    @functools.wraps(method)
    def wrapper(self, args, **kwargs):
        
        value, update_op = method(self, args, **kwargs)
        K.get_session().run(tf.local_variables_initializer())
        with tf.control_dependencies([update_op]):
            value = tf.identity(value)
        return value
    return wrapper

# create model
model = Sequential()
model.add(Dense(40, input_dim=42, activation='relu'))
model.add(Dense(30, activation='sigmoid'))
model.add(Dense(10, activation='sigmoid'))
model.add(Dense(5, activation='sigmoid'))
model.add(Dense(3, activation='softmax'))

#advanced metrics
precision = as_keras_metric(tf.metrics.precision)
recall = as_keras_metric(tf.metrics.recall)

model.compile(loss='mean_squared_error', optimizer="adam", metrics=['accuracy', precision, recall])

history = model.fit(train_input, train_label, epochs=20, batch_size=10, verbose=2)#edit epochs!
scores = model.evaluate(train_input, train_label)

predictions_train = model.predict(train_input)
#format predictions into [0, 2] format
formattedPred = []
for pred in predictions_train:
    regList = np.ndarray.tolist(pred)
    formattedPred.append(regList.index(max(regList)))
#format labels into [0, 2] format
formattedLabels = []
for nprow in train_label:
    row = nprow.tolist()
    formattedLabels.append(row.index(max(row)))

cm_train = confusion_matrix(formattedPred,formattedLabels)
#NaN regulation
prec = cm_train[0][0] / (cm_train[0][0] + cm_train[0][1] + cm_train[0][2]) * 100
rec = cm_train[0][0] / (cm_train[0][0] + cm_train[1][0] + cm_train[2][0]) * 100
print("\ntrain-set precision for class win: %.2f%%" % (prec))
print("train-set recall for class win: %.2f%%" % (rec))

predictions_test = model.predict(test_input)
#format predictions into [0, 2] format
formattedPred = []
for pred in predictions_test:
    regList = np.ndarray.tolist(pred)
    formattedPred.append(regList.index(max(regList)))
#format labels into [0, 2] format
formattedLabels = []
for nprow in test_label:
    row = nprow.tolist()
    formattedLabels.append(row.index(max(row)))

cm_test = confusion_matrix(formattedPred,formattedLabels)
#NaN regulation
prec = cm_test[0][0] / (cm_test[0][0] + cm_test[0][1] + cm_test[0][2]) * 100
rec = cm_test[0][0] / (cm_test[0][0] + cm_test[1][0] + cm_test[2][0]) * 100
print("\ntest-set precision for class win: %.2f%%" % (prec))
print("test-set precision for class win: %.2f%%" % (rec))

import matplotlib.pyplot as plt

# Plot training & validation loss values
plt.plot(history.history['loss'])
# plt.plot(history.history  ['val_loss'])
plt.title('Model loss')
plt.ylabel('Loss')
plt.xlabel('Epoch')
plt.legend(['Train', 'Test'], loc='upper left')
plt.show()

# Plot training and validation confusion matrix
labels = ['win','draw','loss']

def plot_confusion_matrix(cm, classes, normalize=False, title='Confusion matrix', cmap=plt.cm.Blues):
    np.set_printoptions(precision=2)

    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        cm.data = np.nan_to_num(cm.data)
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    print(cm)

    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, format(cm[i, j], fmt),
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

    plt.ylabel('True label')
    plt.xlabel('Predicted label')
    plt.tight_layout()
    plt.show()

plot_confusion_matrix(cm_train, classes=labels, title='Train-set confusion matrix', normalize=True)
plot_confusion_matrix(cm_test, classes=labels, title='Test-set confusion matrix', normalize=True)